package inf101v22.mockexam.set;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Set<T> implements ISet {

    List<T> set = new ArrayList<>();
    
    @Override
    public Iterator iterator() {
        return set.iterator();
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public void add(Object element) {
        if (!(set.contains(element))){
            set.add((T) element);
        }
    }

    @Override
    public void addAll(Iterable other) {
        for (Object object : other) {
            this.add(object);
        }
    }

    @Override
    public void remove(Object element) {
        if (set.contains(element)){
            set.remove(element);
        }
    }

    @Override
    public boolean contains(Object element) {
        return set.contains(element);
    }

    @Override
    public ISet<T> union(ISet other) {
        ISet<T> union = new Set<>();
        union.addAll(set);
        union.addAll(other);
        return union;
    }

    @Override
    public ISet<T> intersection(ISet other) {
        ISet<T> intersection = new Set<>();
        for (Object object : set) {
            if (other.contains(object)){
                intersection.add((T) object);
            }
        }
        return intersection;
    }

    @Override
    public ISet<T> complement(ISet other) {
        ISet<T> complement = new Set<>();
        for (Object object : set) {
            if (!(other.contains(object))){
                complement.add((T) object);
            }
        }
        return complement;
        
    }

    @Override
    public ISet copy() {
        ISet<T> copy = new Set<>();
        for (T item : set) {
            copy.add(item);
        }
        return copy;
    }
    
}
